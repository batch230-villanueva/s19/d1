console.log("test");

//What are conditional statements?

//Conditional statements allow us to control the flow of our program. It allows us to run a statement/instruction if a condition is met or run another separate instruction if otherwise.

// [SECTION] if, else if and else Statement

let numA = -1;

if(numA < 0){
    console.log("Hello");
}

console.log(numA < 0);
//results to true and so, the if statement was run.

numA = 0;

if(numA <= 0) {
    console.log(`Hello again if numA is lesser than 0`);
}

let city = "New York";

if (city === 'New York') {
    console.log("Welcome to New York City");
}

let numH = 1;

if (numA < 0){
    console.log("Hello");
}
else if(numH > 0){
    console.log('Hi');
}

//We were able to run the else if() statement after we evaluated that the if condition was failed.
//If the if() condition was passed and run, we will no longer evaluate to else if() and end the process there.

city = 'Tokyo';

if(city === "Tokyo"){
    console.log("Welcome to Tokyo, Japan");
}
else if (city === "New York"){
    console.log("Welcome to New York City!");
}

/* 
    - Executes a statement if all other conditions are false
    - The "else" statement is optional and can be added to capture any other result to change the flow of a program
*/

// if(numA > 0){
//     console.log("Hello");
// } else if (numH === 0) {
//     console.log("Hi");
// } else if (numA === 100) {
//     console.log("Yehey!");
// } else {
//     console.log("Nah");
// }

// else {
//     console.log("I have no if statement");
// } will not run because there is no if statement

let sheLovesMe = false;

let sadboiArr = [
    "Hahahaha okay lang, hindi naman masakit.",
    "Hindi hahahaha okay lang, sanay na ako.",
    "Sino ba naman ako para seryosohin mo diba?",
]

if(sheLovesMe === true){
    console.log("Hi, is it me you're looking for?")
} else {
    sadboiArr.forEach(statement => console.log(statement));
}

// else if (numH === 0){
//     console.log("I don't have an if statement, either");
// }

function determineTyphoonIntensity(windSpeed){
    if (windSpeed < 30){
        return 'not a typhoon yet';
    } else if (windSpeed <= 61){
        return 'tropical depression detected';
    } else if (windSpeed >= 62 && windSpeed <= 88){
        return 'tropical storm detected';
    } else if (windSpeed >= 89 || windSpeed <= 117) {
        return 'Severe tropical storm detected';
    } else {
        return 'typhoon detected';
    }
}

let message = determineTyphoonIntensity(2);
console.warn(message);

// [SECTION] Truthy and Falsy

/* 
    - In JavaScript a "truthy" value is a value that is considered true when encountered in a Boolean context
    - Values are considered true unless defined otherwise
    - Falsy values/exceptions for truthy:
        1. false
        2. 0
        3. -0 (there is no negative zero, we don't usually want to have a negative zero value in our output)
        4. ""
        5. null
        6. undefined
        7. NaN (Not a Number), 
            example where we will encounter NaN:  console.log('six'/12);
*/

if(true) {
    console.log('truthy');
}

if(1) {
    console.log('truthy');
}

if([]) {
    console.log('truthy');
}

if(false) {
    console.log('falsy');
}

if(0) {
    console.log('falsy');
}

if(undefined) {
    console.log('falsy');
}

let ternaryResult = (1 < 18) ? true : false;
console.log("Result of Ternary Operator: " + ternaryResult);

function isOfLegalAge(){
    name = 'John';
    return 'You are of legal age';
}

function isUnderage(){
    name = 'Jane';
    return 'You are under the legal age';
}

// let age = parseInt(prompt("What is your age?"));
// console.log(age);
// let legalAge = (age >= 18) ? isOfLegalAge() : isUnderage();
// console.log(legalAge);

// [SECTION] Switch Statement
/* 
    -The switch statement evaluates an expression and matches the expression's value to a case clause. The switch will then execute the statements associated with that case, as well as statements in cases that follow the matching case.
    - Can be used as an alternative to an if, "else if and else" statement where the data to be used in the condition is of an expected input
    - The ".toLowerCase()" function/method will change the input received from the prompt into all lowercase letters ensuring a match with the switch case conditions if the user inputs capitalized or uppercased letters
    - The "expression" is the information used to match the "value" provided in the switch cases
    - Variables are commonly used as expressions to allow varying user input to be used when comparing with switch case values
    - Switch cases are considered as "loops" meaning it will compare the "expression" with each of the case "values" until a match is found
    - The "break" statement is used to terminate the current loop once a match has been found
    - Removing the "break" statement will have the switch statement compare the expression with the values of succeeding cases even if a match was found
    - Syntax
        switch (expression) {
            case value:
                statement;
                break;
            default:
                statement;
                break;
        }
*/

// let day = prompt("What day of the week is it today?").toLowerCase();

// switch (day) {
//     case "monday":
//         console.log("The color of the day is red");
//         break;
//     case "tuesday":
//         console.log("The color of the day is orange");
//         break;
//     case "wednesday":
//         console.log("The color of the day is yellow");
//         break;
//     case "thursday":
//         console.log("The color of the day is green");
//         break;
//     case "friday":
//         console.log("The color of the day is blue");
//         break;
//     case "saturday":
//         console.log("The color of the day is indigo");
//         break;
//     case "sunday":
//         console.log("The color of the day is violet");
//         break;
//     default:
//         console.log("That is not a day of the week :/");
//         break;
// }

// [SECTION] Try-Catch-Finally Statement
/*
    - "try catch" statements are commonly used for error handling
    - There are instances when the application returns an error/warning that is not necessarily an error in the context of our code
    - These errors are a result of an attempt of the programming language to help developers in creating efficient code
    - They are used to specify a response whenever an exception/error is received
    - It is also useful for debugging code because of the "error" object that can be "caught" when using the try catch statement
    - In most programming languages, an "error" object is used to provide detailed information about an error and which also provides access to functions that can be used to handle/resolve errors to create "exceptions" within our code
    - The "finally" block is used to specify a response/action that is used to handle/resolve errors
*/

function showIntensityAlert(windSpeed) {
    try{
        alert(determineTyphoonIntensity(windSpeed));
    }
    catch(error){
        console.warn(error.message);
    }
    finally {
        // Continue execution of code regardless of success and failure of code execution in the 'try' block to handle/resolve errors
        alert(`Intensity updates will show new alert`);
    }

    console.log(`Will still run`);
}

showIntensityAlert(16)
